## Podrobné zadání SP
Na základě nadpisu článku nebo celého článku automaticky vyberte vhodný obrázek. Pro trénování a testování použijte články z Wikipedie, které často obsahují ilustrační obrázky.

Úprava zadání: místo wikipedie byly použity stránky serveru novinky.cz

## Pokyny k dotažení potřebných velkých dat

Tento repozitář obsahuje soubor data.zip, který obsahuje soubor data.txt, který obsahuje veškerá potřebaná data. Model určuje pouze id obrázku, samotn obrázky tedy není třeba uploadovat. Samožřejmě pro hezký výsledek se obrázky hodí. Mohu vám poslat link na stažení. Napište mi email.


## Pokyny ke spuštění

Tento repozitář obsahuje jupter notebook, který obsahuje veškerý kód pro vytvoření modelu. V noteboku jsou popsané detalnější informace.